# frozen_string_literal: true

module Rubocop
  module Mailo
    VERSION = '0.4.2'
  end
end
