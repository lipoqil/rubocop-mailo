# Rubocop::Mailo

This is my set of rules for [Rubocop](https://github.com/rubocop-hq/rubocop). Rubocop supports [configuration
distribution via http](https://rubocop.readthedocs.io/en/latest/configuration/#inheriting-configuration-from-a-remote-url),
but it has proven problematic in some environments, hence this gem.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rubocop-mailo'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rubocop-mailo

Add file _.rubocop.yml_ to root of you Ruby project. The file should have following content

~~~yaml
inherit_gem:
  rubocop-mailo: rubocop.yaml
~~~

## Usage

Run `rubocop`. If you wanna verify configuration from this project is actually used, You can run `rubocop --debug`

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/lipoqil/rubocop-mailo.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
