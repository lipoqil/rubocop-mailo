# frozen_string_literal: true

RSpec.describe Rubocop::Mailo do
  it 'has a version number' do
    expect(Rubocop::Mailo::VERSION).not_to be nil
  end
end
