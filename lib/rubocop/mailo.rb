# frozen_string_literal: true

require 'rubocop/mailo/version'

module Rubocop
  module Mailo
    class Error < StandardError; end
    # Your code goes here...
  end
end
