# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rubocop/mailo/version'

Gem::Specification.new do |spec|
  spec.name          = 'rubocop-mailo'
  spec.version       = Rubocop::Mailo::VERSION
  spec.authors       = ['Mailo Světel']
  spec.email         = ['mailo@rooland.cz']

  spec.summary       = 'Set of my default Rubocop rules'
  spec.description   = 'Set of my default Rubocop rules'
  spec.homepage      = 'https://gitlab.com/lipoqil/rubocop-mailo'
  spec.license       = 'MIT'

  spec.required_ruby_version = '>= 3.2'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = 'https://rubygems.org'

    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = spec.homepage
    spec.metadata['changelog_uri'] = "#{spec.homepage}/blob/master/CHANGELOG"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
          'public gem pushes.'
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = %w[rubocop.yaml README.md]
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.post_install_message = <<~PIM
    ## #{spec.name}
    Add file .rubocop.yml to root of you Ruby project. The file should have following content

      inherit_gem:
        #{spec.name}: rubocop.yaml
  PIM

  spec.add_dependency 'rubocop', '~> 1.56.1'

  spec.metadata['rubygems_mfa_required'] = 'true'
end
